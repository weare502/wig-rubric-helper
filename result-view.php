<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */
the_post();
$context = Timber::get_context();
$post = new TimberPost();
$context['hero_image'] = $context['options']['assessment_image']['sizes']['xlarge'];
$templates = array( 'assessment-result.twig' );
weare502_modify_footer_cta( $post, $context );

Timber::render( $templates, $context );