/* global _ jQuery */

var AssessmentApp = new Vue({
    el: '#page',
    data: {
        client: '',
        rawContent: '',
        content: '',
        tabs: [],
        tab: '',
        currentTab: 0,
    },
    methods: {
        changeTab(index, scrollTop){
            scrollTop = scrollTop ? true : false;
            this.currentTab = index;
            if ( scrollTop ){
                jQuery('html, body').animate({
                    scrollTop: jQuery(".report").offset().top - 100
                }, 1000);
            }
        }
    },
    computed: {
        currentTabContent: function(){
            if ( this.tabs.length ){
                return this.tabs[this.currentTab].content;
            }
            return '';
        }
    },
    mounted: function(){
        var data = _.filter(location.pathname.split('/'), function(s){
            return s !== "";
        });
        
        // index 1 will always be our string of data
        var url = atob( data[1] );
        var that = this;
        jQuery.ajax({
            url: url,
            success: function(response){
                console.log(response);
                that.client = response.title.rendered;
                that.rawContent = response.content.rendered;
            },
            error: function(response){
                console.error('Something went wrong', response);
            }
        });
    },
    watch: {
        rawContent: function(content){
            var html = jQuery.parseHTML(content);
            var $tabs = jQuery('<div>').append(html).find('.tab-start');
            console.log(html);
            console.log($tabs);
            var that = this;
            $tabs.each(function(){
                that.tabs.push({
                    title: jQuery(this).attr('data-tab-title'),
                    content: jQuery(this).html()
                });
            });
        }
    }
});
