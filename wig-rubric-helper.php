<?php
/**
 * Plugin Name: WIG Rubric Helper
 */

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

add_action('after_setup_theme', function(){

    Timber::$locations = array(
        plugin_dir_path(__FILE__ )
    );
});

add_action('init', function(){
    global $wp_rewrite;

    // WeAre502.com Rules
    add_rewrite_tag( '%assessments%', '([^&]+)' );
    add_rewrite_rule( '^assessments/([^/]*)/?', 'index.php?assessments=$matches[1]','top' );
    // add_rewrite_endpoint('bears', EP_PERMALINK | EP_PAGES );
});

add_filter('template_include', function($template){

    global $wp;
    global $wp_query;
    // var_dump($wp->query_vars); die();
    if ( isset($wp->query_vars['assessments']) ){
        $template = plugin_dir_path( __FILE__ ) . 'result-view.php';
        $wp_query->is_home = false;
    }

    return $template;

}, 99);

add_action('wp', function(){
    global $wp;
    if ( array_key_exists( 'assessments', $wp->query_vars ) ){
        wp_enqueue_script( 'vuejs', 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js', '20180101', true );
        wp_enqueue_script( 'wig-result-view', plugin_dir_url(__FILE__ ) . 'result-view.js', array('jquery', 'underscore', 'vuejs'), '20180102', true );
        wp_enqueue_style( 'wig-result-view', plugin_dir_url( __FILE__ ) . 'result-view.css', array(), '20180103' );
    }
});
